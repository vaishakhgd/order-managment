package com.egen.model;

import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonProperty;



@Entity
@Table(name = "payments")
public class Payment {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="payment_id",unique=true)
	public int payment_id;
	
	@Column(name="payment_type")
	public String payment_type;
	
	
	@Basic
	@Column(name="payment_time")
	public Long time;
	
	@Column(name="payment_confirmation_no",unique=true)
	public String payment_confirmation_no;
	
	
	@Column(name="payment_amount",nullable=false)
	public Double payment_amount;
	
	
	
	public Payment() {}
	
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false,
            cascade = CascadeType.ALL)
    @JoinColumn(name = "order_id")
	public Order orderObjInPayment;
	
	
	 public Payment(  String paymentType) {
	       
	        
	        this.payment_type = paymentType;
	        
	        
	       
	    }
	 
	 
	 
	    
	    
	    
	    public void setPaymentType(String paymentType) {
			this.payment_type = paymentType;
	    }
	    
	    
	    public String getPaymentType() {
			return payment_type;
	    	
	    }
	    
	    
	//other setters and getters
	    @OneToOne(mappedBy="payment", fetch = FetchType.LAZY,
	            cascade = CascadeType.ALL)
	 public Billing billingObjInPayment;
	   
	
	

}
