package com.egen.model;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;




@Entity
@Table(name = "item")
public class Item {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="item_id",unique=true)
	public int item_id;
	
	@Column(name="item_name")
	public String item_name;
	
	
	@Basic
	@Column(name="item_qty",nullable=false)
	public int item_qty;
	
	@Column(name="item_price",nullable=false)
	public Double item_price;
	
	//@ManyToOne(fetch = FetchType.LAZY, optional = false,cascade = CascadeType.ALL)
   // @JoinColumn(name = "order_id")
	//public Order orderObjInItem;
	
	
	 @OneToOne(mappedBy="item", fetch = FetchType.LAZY,
	            cascade = CascadeType.ALL)
	 public OrderItems orderItems;
		
	
	
	public Item() {}
	
	
	
	
	
}
