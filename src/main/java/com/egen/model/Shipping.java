package com.egen.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;




@Entity
@Table(name = "shipping")
public class Shipping {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="shipping_id",unique=true)
	public int shipping_id;
	
	@Column(name="shipping_address_line1",nullable=false)
	public String shipping_address_line1;
	
	@Column(name="shipping_address_line2")
	public String shipping_address_line2;
	
	@Column(name="shipping_city",nullable=false)
	public String shipping_city;
	
	@Column(name="shipping_state",nullable=false)
	public String shipping_state;
	
	@Column(name="shipping_zip",nullable=false)
	public String shipping_zip;
	
	@Column(name="shipping_charges")
	public Double shipping_charges;
	
	

	
	
	 @OneToOne(fetch = FetchType.LAZY,
	            cascade = CascadeType.ALL)
	 @JoinColumn(name="order_shipping_id")
	public Order order;
	
	 @OneToOne(mappedBy="shipping", fetch = FetchType.LAZY,
	            cascade = CascadeType.ALL)
	 public ShippingInfo shippingInfoObjInShipping;
	
	

}
