package com.egen.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;



@Entity
@Table(name = "orders")
public class Order {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;
	
	
	@Column(name="order_id",unique=true)
	public String orderId;
	
	
	
	
	@Column(name="order_tax",nullable=false)
	public Double order_tax;
	
	@Column(name="create_time")
	public Long create_time;
	
	@Column(name="modified_time")
	public Long modified_time;
	
	@Column(name="customer_id",nullable=false)
	public String customer_id;
	
	@Column(name="order_status")
	public String order_status;
	
	@Column(name="order_total",nullable=false)
	public Double order_total;
	
	@Column(name="order_sub_total",nullable=false)
	public Double order_sub_total;
	
	
	
	public Order() {}
	
	
	@OneToMany(mappedBy = "orderObjInPayment", fetch = FetchType.LAZY,
	            cascade = CascadeType.ALL)
	public List<Payment> payments = new ArrayList<>();
	 
	 
	// @OneToMany(mappedBy = "orderObjInItem", fetch = FetchType.LAZY,
	//            cascade = CascadeType.ALL)
	//public List<Item> items = new ArrayList<>();
	 
	 
	 @OneToOne(mappedBy="order", fetch = FetchType.LAZY,
	            cascade = CascadeType.ALL)
	 public Shipping shippingObjInOrder;
	 
	 
	 @OneToOne(mappedBy="order", fetch = FetchType.LAZY,
	            cascade = CascadeType.ALL)
	 public OrderItems orderItems;
    
	 
	
    
    
    
	 public void setOrderId(String orderId) {
		this.orderId = orderId;
    }
    
    
    public String getOrderId() {
		return orderId;
    	
    }
    	
    
    public Order( String orderId) {
       
                this.orderId=orderId;
        
    }
    
    
//other setters and getters
    
  
	
}
