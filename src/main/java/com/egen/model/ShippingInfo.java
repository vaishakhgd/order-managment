package com.egen.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "shippinginfo")
public class ShippingInfo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="shipping_info_id",unique=true)
	public int shipping_info_id;
	
	@Column(name="vendor_id")
	public String vendor_id;
	
	
	@Column(name="delivery_type")
	public String delivery_type;
	
	@OneToOne(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
 @JoinColumn(name="shipping_id")
public Shipping shipping;

}
