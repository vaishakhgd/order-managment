package com.egen.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "orderitems")
public class OrderItems {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;
	
	
	@OneToOne(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
 @JoinColumn(name="item_id")
public Item item;
	
	
	
	
	 @OneToOne(fetch = FetchType.LAZY,
	            cascade = CascadeType.ALL)
	 @JoinColumn(name="order_id")
	public Order order;
	
}