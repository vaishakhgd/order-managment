package com.egen.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "billing")
public class Billing {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="billing_id",unique=true)
	public int billing_id;
	
	@Column(name="billing_address_line1",nullable=false)
	public String billing_address_line1;
	
	@Column(name="billing_address_line2")
	public String billing_address_line2;
	
	@Column(name="billing_city",nullable=false)
	public String billing_city;
	
	@Column(name="billing_state",nullable=false)
	public String billing_state;
	
	@Column(name="billing_zip",nullable=false)
	public String billing_zip;
	
	
	@OneToOne(fetch = FetchType.LAZY,
	            cascade = CascadeType.ALL)
	 @JoinColumn(name="order_payment_id")
	
	public Payment payment;
	
	
	

}
