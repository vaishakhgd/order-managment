package com.egen;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
public class JPAConfig {

	@Bean
	public LocalContainerEntityManagerFactoryBean emf() {
		//TODO: configure emf
		//return null;
		 LocalContainerEntityManagerFactoryBean em 
	        = new LocalContainerEntityManagerFactoryBean();
	      em.setDataSource(this.dataSource());
	      em.setPackagesToScan(new String[] { "com.egen.model" });

	      JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
	      em.setJpaVendorAdapter(vendorAdapter);
	      em.setJpaProperties(this.jpaProperties());

	      return em;
	}

	@Bean
	public DataSource dataSource() {
		//TODO: configure data source bean
		//return  null;
		 DriverManagerDataSource dataSource = new DriverManagerDataSource();
		    dataSource.setDriverClassName("org.postgresql.Driver");
		    dataSource.setUrl("jdbc:postgresql://localhost:5432/vaish?useJDBCCompliantTimezoneShift=true");
		    dataSource.setUsername( "vaish" );
		    dataSource.setPassword( "password" );
		    return dataSource;
	}

	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
		//TODO: configure transaction manager
		//return null;
		 JpaTransactionManager transactionManager = new JpaTransactionManager();
		    transactionManager.setEntityManagerFactory(emf);

		    return transactionManager;
	}

	private Properties jpaProperties() {
		//TODO: configure jpa properties
		 Properties properties = new Properties();
		    properties.setProperty("hibernate.hbm2ddl.auto", "create");
		    properties.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
		    properties.setProperty("hibernate.show_sql", "true");
		    return properties;
	}
}
