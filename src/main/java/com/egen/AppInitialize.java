package com.egen;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class AppInitialize extends AbstractAnnotationConfigDispatcherServletInitializer{
/**
 * implement the following methods
 */

	@Override
	protected Class<?>[] getRootConfigClasses() {
		//return new Class<?>[0];
		return new Class[] {AppConfig.class};

	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[0];
		//return new Class[] {SpringMvcConfig.class};
	}

	@Override
	protected String[] getServletMappings() {
		 return new String[]{"/order/*"};
		//return new String[] {};
		 //telling DispatcherServlet to listen to globally Map requests coming  from /order
	}
}
